import os

import xlrd
import xlwt
import docx
from xlutils.copy import copy

from abc import ABC, abstractmethod


class Builder(ABC):
    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def create_file(self):
        pass

    @abstractmethod
    def create_table(self):
        pass

    @abstractmethod
    def get_result(self):
        pass


class ExcelBuilder(Builder):
    def __index__(self):
        self.reset()

    def reset(self) -> None:
        self._product = Excel()

    def create_file(self):
        self._product.create_file()

    def create_table(self):
        self._product.create_table()

    def get_result(self):
        product = self._product
        self.reset()
        return product


class WordBuilder(Builder):
    def __index__(self):
        self.reset()

    def reset(self) -> None:
        self._product = Word()

    def create_file(self):
        self._product.create_file()

    def create_table(self):
        self._product.create_table()

    def get_result(self):
        product = self._product
        self.reset()
        return product


class Director:
    def __init__(self) -> None:
        self._builder = None

    def get_builder(self) -> Builder:
        return self._builder

    def set_builder(self, builder: Builder) -> None:
        self._builder = builder

    def build_create(self) -> None:
        self._builder.reset()
        self._builder.create_file()

    def build_create_table(self) -> None:
        self._builder.reset()
        self._builder.create_file()
        self._builder.create_table()


class File(ABC):
    @abstractmethod
    def create_table(self):
        pass

    @abstractmethod
    def print_table(self):
        pass


class FileCreator(ABC):
    @abstractmethod
    def create_file(self) -> File:
        pass

    def operations(self):
        file = self.create_file()
        file.print_table()


class ExcelCreator(FileCreator):
    def create_file(self) -> File:
        director = Director()
        builder = ExcelBuilder()
        director.set_builder(builder)
        director.build_create_table()
        return director.get_builder().get_result()


class WordCreator(FileCreator):
    def create_file(self) -> File:
        director = Director()
        builder = WordBuilder()
        director.set_builder(builder)
        director.build_create_table()
        return director.get_builder().get_result()


class Excel(File):
    def __init__(self, filename='test.xls'):
        self.filename = filename

    def is_exist_file(self) -> bool:
        return os.path.exists(self.filename)

    def create_file(self) -> None:
        wb = xlwt.Workbook()
        wb.add_sheet('First sheet')
        wb.save(self.filename)

    def open_file(self) -> xlrd:
        if not self.is_exist_file():
            self.create_file()
        return xlrd.open_workbook(self.filename, formatting_info=True)

    def delete_file(self):
        os.remove(self.filename)

    def create_table(self):
        wb = self.open_file()
        wb = copy(wb)
        ws = wb.get_sheet(0)
        for i in range(10):
            for j in range(10):
                ws.write(i, j, (i + 1) * (j + 1))
        wb.save(self.filename)

    def print_table(self):
        wb = self.open_file()
        ws = wb.sheet_by_index(0)
        for rownum in range(ws.nrows):
            row = ws.row_values(rownum)
            for cel in row:
                print(cel, end=' ')
            print()


class Word(File):
    def __init__(self, filename='test.docx'):
        self.filename = filename

    def is_exist_file(self) -> bool:
        return os.path.exists(self.filename)

    def create_file(self) -> None:
        doc = docx.Document()
        doc.save(self.filename)

    def open_file(self) -> docx:
        if not self.is_exist_file():
            self.create_file()
        return docx.Document(self.filename)

    def delete_file(self):
        os.remove(self.filename)

    def create_table(self):
        doc = self.open_file()
        table = doc.add_table(0, 10)
        for i in range(10):
            row_cells = table.add_row().cells
            for j in range(10):
                row_cells[j].text = str((i + 1) * (j + 1))
        doc.save(self.filename)

    def print_table(self):
        doc = self.open_file()
        tabl = doc.tables[0]
        rows = len(tabl.rows)
        columns = len(tabl.columns)
        for rownum in range(rows):
            for celnum in range(columns):
                print(tabl.cell(rownum, celnum).text, end='\t')
            print()


def client_code(creator: FileCreator):
    creator.operations()


if __name__ == "__main__":
    excelCreator = ExcelCreator()
    wordCreator = WordCreator()
    client_code(wordCreator)

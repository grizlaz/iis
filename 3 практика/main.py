from __future__ import annotations
from abc import ABC, abstractmethod


class Screen(ABC):
    """
    Интерфейс Продукта объявляет операции, которые должны выполнять все
    конкретные продукты.
    """

    @abstractmethod
    def operation(self) -> str:
        pass

    @abstractmethod
    def accept(self, v: Visitor) -> str:
        pass


"""
Конкретные Продукты предоставляют различные реализации интерфейса Продукта.
"""


class TV(Screen):
    def operation(self) -> str:
        return "TV"

    def accept(self, v: Visitor) -> str:
        return v.visit_tv(self)


class Monitor(Screen):
    def operation(self) -> str:
        return "Monitor"

    def accept(self, v: Visitor) -> str:
        return v.visit_monitor(self)


class Visitor(ABC):
    """
    Интерфейс Посетителя объявляет набор методов посещения, соответствующих
    классам компонентов. Сигнатура метода посещения позволяет посетителю
    определить конкретный класс компонента, с которым он имеет дело.
    """

    @abstractmethod
    def visit_tv(self, tv: TV) -> str:
        pass

    @abstractmethod
    def visit_monitor(self, monitor: Monitor) -> str:
        pass


class ConcreteVisitor(Visitor):
    def visit_tv(self, tv: TV) -> str:
        return "Visit TV"

    def visit_monitor(self, monitor: Monitor) -> str:
        return "Visit Monitor"


class ScreenCreator(ABC):
    """
    Класс Создатель объявляет фабричный метод, который должен возвращать объект
    класса Продукт. Подклассы Создателя обычно предоставляют реализацию этого
    метода.
    """

    @abstractmethod
    def factory_method(self):
        """
        Обратите внимание, что Создатель может также обеспечить реализацию
        фабричного метода по умолчанию.
        """
        pass

    def some_operation(self) -> Screen:
        """
        Также заметьте, что, несмотря на название, основная обязанность
        Создателя не заключается в создании продуктов. Обычно он содержит
        некоторую базовую бизнес-логику, которая основана на объектах Продуктов,
        возвращаемых фабричным методом. Подклассы могут косвенно изменять эту
        бизнес-логику, переопределяя фабричный метод и возвращая из него другой
        тип продукта.
        """

        # Вызываем фабричный метод, чтобы получить объект-продукт.
        product = self.factory_method()
        return product
        # # Далее, работаем с этим продуктом.
        # result = f"Creator: The same creator's code has just worked with {product.operation()}"
        #
        # return result


"""
Конкретные Создатели переопределяют фабричный метод для того, чтобы изменить тип
результирующего продукта.
"""


class TVCreator(ScreenCreator):
    """
    Обратите внимание, что сигнатура метода по-прежнему использует тип
    абстрактного продукта, хотя фактически из метода возвращается конкретный
    продукт. Таким образом, Создатель может оставаться независимым от конкретных
    классов продуктов.
    """

    def factory_method(self) -> TV:
        return TV()


class MonitorCreator(ScreenCreator):
    def factory_method(self) -> Monitor:
        return Monitor()


def client_code(creator: ScreenCreator) -> None:
    """
    Клиентский код работает с экземпляром конкретного создателя, хотя и через
    его базовый интерфейс. Пока клиент продолжает работать с создателем через
    базовый интерфейс, вы можете передать ему любой подкласс создателя.
    """

    product = creator.some_operation()
    v = ConcreteVisitor()
    print(f"Результат фабричного метода: {product.operation()}")
    print(f"Результат посетителя: {product.accept(v)}")


if __name__ == "__main__":
    print("Приложение: запускается с помощью TVCreator.")
    client_code(TVCreator())
    print("\n")

    print("Приложение: запускается с помощью MonitorCreator.")
    client_code(MonitorCreator())

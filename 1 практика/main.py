from abc import ABCMeta, abstractmethod


class BaseUnit(metaclass=ABCMeta):

    def __init__(self, speed: int):
        self._speed = speed

    def hit_and_run(self):
        self._move('вперед')
        self._stop()
        self._attack()
        self._move('назад')

    @abstractmethod
    def _attack(self):
        pass

    @abstractmethod
    def _stop(self):
        pass

    def _move(self, direction: str):
        self._output('движется {} со скоростью {}'.format(direction, self._speed))

    def _output(self, message: str):
        print('Роль {} {}'.format(self.__class__.__name__, message))


class Archers(BaseUnit):

    def _attack(self):
        self._output('обстреливает')

    def _stop(self):
        self._output('останавливается в 100 шагах от врага')


class Knight(BaseUnit):

    def _attack(self):
        self._output('врезается во вражеский строй')

    def _stop(self):
        self._output('возрвращается в оборону')


class Wizard(BaseUnit):

    def _attack(self):
        self._output('кидает фаербол')

    def _stop(self):
        self._output('останавливается')


class Player:

    def __init__(self, role):
        self.role = role

    def attack(self):
        self.role._attack()

    def stop(self):
        self.role._stop()


if __name__ == '__main__':
    arc = Archers(5)
    # wiz = Wizard(5)
    player = Player(arc)
    player.attack()
    player.stop()

from unittest import TestCase
from main import Player, Wizard


class TestWizard(TestCase):
    def test__attack(self):
        wiz = Wizard(5)
        player = Player(wiz)
        self.assertIsNone(player.attack())

from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Any, Optional


class Handler(ABC):
    """
    Интерфейс Обработчика объявляет метод построения цепочки обработчиков. Он
    также объявляет метод для выполнения запроса.
    """

    @abstractmethod
    def set_next(self, handler: Handler) -> Handler:
        pass

    @abstractmethod
    def handle(self, request) -> Optional[str]:
        pass


class AbstractHandler(Handler):
    """
    Поведение цепочки по умолчанию может быть реализовано внутри базового класса
    обработчика.
    """
    _next_handler: Handler = None

    def set_next(self, handler: Handler) -> Handler:
        self._next_handler = handler
        return handler

    @abstractmethod
    def handle(self, request: Any) -> str:
        if self._next_handler:
            return self._next_handler.handle(request)
        return f"file is not word or excel, type of file: {type(request)}"


class WordHandler(AbstractHandler):
    def handle(self, request: Any) -> str:
        if type(request) == Word:
            return "Word, file path: " + request.get_path()
        else:
            return super().handle(request)


class ExcelHandler(AbstractHandler):
    def handle(self, request: Any) -> str:
        if type(request) == Excel:
            return "Word, file path: " + request.get_path()
        else:
            return super().handle(request)


class Word:
    _path = "word_file.docx"

    def get_path(self) -> str:
        return self._path


class Excel:
    _path = "excel_file.xls"

    def get_path(self) -> str:
        return self._path


class Client:
    def __init__(self, handler):
        self._handler = handler

    def check_file(self, file):
        return self._handler.handle(file)


def client_code(handler):
    word = Word()
    excel = Excel()
    client = Client(handler)
    print(client.check_file(word))
    print(client.check_file(excel))
    print(client.check_file("123"))


if __name__ == "__main__":
    word_handler = WordHandler()
    excel_handler = ExcelHandler()

    word_handler.set_next(excel_handler)

    client_code(word_handler)
    print("\n")

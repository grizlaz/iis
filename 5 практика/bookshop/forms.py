from django.forms import ModelForm
from bookshop.models import *

class CardForm(ModelForm):
    class Meta:
        model = Card
        # fields = ["id_card", "name", "second_name", "cvv", "time"]
        fields = ["name", "second_name", "cvv", "time"]
        labels = {
            # "id_card": "ID",
            "name": "Имя",
            "second_name": "Фамилия",
            "cvv": "CVV",
            "time": "Срок действия"
        }

class BuyerForm(ModelForm):
    class Meta:
        model = Buyer
        fields = ["id_buyer", "name", "second_name", "mail", "time"]
        labels = {
            # "id_buyer": "ID",
            "name": "Имя",
            "second_name": "Фамилия",
            "mail": "Почта",
            "time": "День рождения",
        }

class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = ["id_author", "name", "second_name", "last_name"]
        labels = {
            "name": "Имя",
            "second_name": "Фамилия",
            "last_name": "Отчество",
        }

class ProviderForm(ModelForm):
    class Meta:
        model = Provider
        fields = ["id_provider", "name", "phone", "adress", "site"]
        labels = {
            "name": "Название",
            "phone": "Телефон",
            "adress": "Адрес",
            "site": "Сайт",
        }

class GenreForm(ModelForm):
    class Meta:
        model = Genre
        fields = ["id_genre", "name"]
        labels = {
            "name": "Название",
        }

class PublisherForm(ModelForm):
    class Meta:
        model = Publisher
        fields = ["id_Publisher", "name", "adress", "phone", "mail", "site"]
        labels = {
            "name": "Название",
            "adress": "Адрес",
            "phone": "Телефон",
            "mail": "Электронная почта",
            "site": "Сайт",
        }

class ShopForm(ModelForm):
    class Meta:
        model = Shop
        fields = ["id_shop", "adress", "phone"]
        labels = {
            "adress": "Адрес",
            "phone": "Телефон",
        }

class DepartmentForm(ModelForm):
    class Meta:
        model = Department
        fields = ["id_department", "id_shop", "name", "about"]
        labels = {
            "id_shop": "Номер магазина",
            "name": "Название",
            "about": "Об отделе",
        }

class StockForm(ModelForm):
    class Meta:
        model = Stock
        fields = ["id_stock", "id_shop", "adress"]
        labels = {
            "id_shop": "Номер магазина",
            "adress": "Адрес",
        }

class AssociateForm(ModelForm):
    class Meta:
        model = Associate
        fields = ["id_associate", "id_stock", "id_shop", "name", "second_name", "last_name", "passport", "wage",
                  "position"]
        labels = {
            "id_stock": "Номер склада",
            "id_shop": "Номер магазина",
            "name": "Имя",
            "second_name": "Фамилия",
            "last_name": "Отчество",
            "passport": "Паспортные данные",
            "wage": "Зарплата",
            "position": "Должность",
        }

class AvailabilityForm(ModelForm):
    class Meta:
        model = Availability
        fields = ["id_availability", "id_stock", "name", "quantity"]
        labels = {
            "id_stock": "Номер скалада",
            "name": "Наименование",
            "quantity": "Количество",
        }

class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ["id_order", "id_associate", "id_card", "id_buyer", "date"]
        labels = {
            "id_associate": "Сотрудник",
            "id_card": "Карта",
            "id_buyer": "Покупатель",
            "date": "Дата заказа",
        }

class DeliverForm(ModelForm):
    class Meta:
        model = Deliver
        fields = ["id_deliver", "id_order", "adress", "time", "status"]
        labels = {
            "id_order": "Номер заказа",
            "adress": "Адрес доставки",
            "time": "Время доставки",
            "status": "Статус",
        }

class BasketForm(ModelForm):
    class Meta:
        model = Basket
        fields = ["id_basket", "id_order", "quantity", "price"]
        labels = {
            "id_order": "Номер заказа",
            "quantity": "Количество",
            "price": "Стоимость",
        }

class BookForm(ModelForm):
    class Meta:
        model = Book
        # fields = ["id_book", "id_author", "id_provider", "id_genre", "id_publisher", "id_basket", "name", "language",
        #           "price", "cover"]
        fields = ["id_book", "id_author", "id_provider", "id_genre", "id_publisher", "name", "language",
                  "price"]
        labels = {
            "id_author": "Автор",
            "id_provider": "Поставщик",
            "id_genre": "Жанр",
            "id_publisher": "Издетель",
            # "id_basket": "Номер корзины",
            "name": "Название",
            "language": "Язык",
            "price": "Цена",
            # "cover": "Обложка",
        }

class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = ["id_review", "id_book", "text"]
        labels = {
            "id_book": "Книга",
            "text": "Отзыв",
        }

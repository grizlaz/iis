from django.shortcuts import render, redirect
from bookshop.forms import *
from django.contrib.auth import authenticate, logout


un = '1'
pw = '2'
# pw = '2'


# Create your views here.
def index(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        return render(request, "bookshop/index.html")
    else:
        return redirect('auth')


def log_out(request):
    global un
    global pw
    un = '1'
    pw = '2'
    logout(request)
    return render(request, 'bookshop/index.html')


def auth(request):
    if request.method == 'POST':
        global un
        global pw
        un = request.POST['username']
        pw = request.POST['pass']
        user = authenticate(username=un, password=pw)
        if user is not None:
            return redirect('bookshop')
        else:
            return redirect('auth')
    return render(request, 'bookshop/auth.html', locals())


# def index(request):
#     return render(request, "bookshop/index.html", locals())

def thanks(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        return render(req, "bookshop/thanks.html", locals())
    else:
        return redirect('auth')


def show_card(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Имя", "Фамилия", "CVV", "Срок действия"]
        obj_list = Card.objects.order_by('id_card')
        temp = "add_card"
        return render(request, "bookshop/show_card.html", locals())
    else:
        return redirect('auth')


def show_associate(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Номер склада", "Номер магазина", "Имя", "Фамилия", "Отчество", "Паспортные данные", "Зарплата", "Должность"]
        obj_list = Associate.objects.order_by('id_associate')
        temp = "add_associate"
        return render(request, "bookshop/show_associate.html", locals())
    else:
        return redirect('auth')


def show_author(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Имя", "Фамилия", "Отчество"]
        obj_list = Author.objects.order_by('id_author')
        temp = "add_author"
        return render(request, "bookshop/show_author.html", locals())
    else:
        return redirect('auth')


def show_availability(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Номер скалада", "Наименование", "Количество"]
        obj_list = Availability.objects.order_by('id_availability')
        temp = "add_availability"
        return render(request, "bookshop/show_availability.html", locals())
    else:
        return redirect('auth')


def show_basket(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Номер заказа", "Количество", "Стоимость"]
        obj_list = Basket.objects.order_by('id_basket')
        temp = "add_basket"
        return render(request, "bookshop/show_basket.html", locals())
    else:
        return redirect('auth')


def show_book(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        # field_list = ["Автор", "Поставщик", "Жанр", "Издетель", "Номер корзины", "Название", "Язык", "Цена", "Обложка"]
        field_list = ["Автор", "Поставщик", "Жанр", "Издетель", "Название", "Язык", "Цена"]
        obj_list = Book.objects.order_by('id_book')
        temp = "add_book"
        return render(request, "bookshop/show_book.html", locals())
    else:
        return redirect('auth')


def show_buyer(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Имя", "Фамилия", "Почта", "День рождения"]
        obj_list = Buyer.objects.order_by('id_buyer')
        temp = "add_buyer"
        return render(request, "bookshop/show_buyer.html", locals())
    else:
        return redirect('auth')


def show_deliver(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Номер заказа", "Адрес доставки", "Время доставки", "Статус"]
        obj_list = Deliver.objects.order_by('id_deliver')
        temp = "add_deliver"
        return render(request, "bookshop/show_deliver.html", locals())
    else:
        return redirect('auth')


def show_department(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Номер магазина", "Название", "Об отделе"]
        obj_list = Department.objects.order_by('id_department')
        temp = "add_department"
        return render(request, "bookshop/show_department.html", locals())
    else:
        return redirect('auth')


def show_genre(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Название"]
        obj_list = Genre.objects.order_by('id_genre')
        temp = "add_genre"
        return render(request, "bookshop/show_genre.html", locals())
    else:
        return redirect('auth')


def show_order(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Сотрудник", "Карта", "Покупатель", "Дата заказа"]
        obj_list = Order.objects.order_by('id_order')
        temp = "add_order"
        return render(request, "bookshop/show_order.html", locals())
    else:
        return redirect('auth')


def show_provider(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Название", "Адрес", "Телефон", "Сайт"]
        obj_list = Provider.objects.order_by('id_provider')
        temp = "add_provider"
        return render(request, "bookshop/show_provider.html", locals())
    else:
        return redirect('auth')


def show_publisher(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Название", "Адрес", "Телефон", "Электронная почта", "Сайт"]
        obj_list = Publisher.objects.order_by('id_Publisher')
        temp = "add_publisher"
        return render(request, "bookshop/show_publisher.html", locals())
    else:
        return redirect('auth')


def show_review(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Книга", "Отзыв"]
        obj_list = Review.objects.order_by('id_review')
        temp = "add_review"
        return render(request, "bookshop/show_review.html", locals())
    else:
        return redirect('auth')


def show_shop(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Адрес", "Телефон"]
        obj_list = Shop.objects.order_by('id_shop')
        temp = "add_shop"
        return render(request, "bookshop/show_shop.html", locals())
    else:
        return redirect('auth')


def show_stock(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list = ["Номер магазина", "Адрес"]
        obj_list = Stock.objects.order_by('id_stock')
        temp = "add_stock"
        return render(request, "bookshop/show_stock.html", locals())
    else:
        return redirect('auth')


def add_card(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Card()
        if req.method == "GET":
            form_obj = CardForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = CardForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_associate(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Associate()
        if req.method == "GET":
            form_obj = AssociateForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = AssociateForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_author(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Author()
        if req.method == "GET":
            form_obj = AuthorForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = AuthorForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_availability(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Availability()
        if req.method == "GET":
            form_obj = AvailabilityForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = AvailabilityForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_basket(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Basket()
        if req.method == "GET":
            form_obj = BasketForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = BasketForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_book(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Book()
        if req.method == "GET":
            form_obj = BookForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = BookForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_buyer(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Buyer()
        if req.method == "GET":
            form_obj = BuyerForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = BuyerForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_deliver(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Deliver()
        if req.method == "GET":
            form_obj = DeliverForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = DeliverForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_department(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Department()
        if req.method == "GET":
            form_obj = DepartmentForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = DepartmentForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_genre(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Genre()
        if req.method == "GET":
            form_obj = GenreForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = GenreForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_order(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Order()
        if req.method == "GET":
            form_obj = OrderForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = OrderForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_provider(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Provider()
        if req.method == "GET":
            form_obj = ProviderForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = ProviderForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_publisher(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Publisher()
        if req.method == "GET":
            form_obj = PublisherForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = PublisherForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_review(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Review()
        if req.method == "GET":
            form_obj = ReviewForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = ReviewForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_shop(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Shop()
        if req.method == "GET":
            form_obj = ShopForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = ShopForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def add_stock(req):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Stock()
        if req.method == "GET":
            form_obj = StockForm(instance=obj)
            return render(req, 'bookshop/add.html', locals())
        else:
            if req.method == "POST":
                form_obj = StockForm(req.POST, instance=obj)
                if form_obj.is_valid():
                    form_obj.save()
                    return redirect('thanks')
    else:
        return redirect('auth')


def edit_associate(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Associate.objects.get(pk=id)
        form = AssociateForm(instance=obj)
        if req.method == "POST":
            form = AssociateForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_author(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Author.objects.get(pk=id)
        form = AuthorForm(instance=obj)
        if req.method == "POST":
            form = AuthorForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_availability(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Availability.objects.get(pk=id)
        form = AvailabilityForm(instance=obj)
        if req.method == "POST":
            form = AvailabilityForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_basket(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Basket.objects.get(pk=id)
        form = BasketForm(instance=obj)
        if req.method == "POST":
            form = BasketForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_book(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Book.objects.get(pk=id)
        form = BookForm(instance=obj)
        if req.method == "POST":
            form = BookForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_buyer(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Buyer.objects.get(pk=id)
        form = BuyerForm(instance=obj)
        if req.method == "POST":
            form = BuyerForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_card(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Card.objects.get(pk=id)
        form = CardForm(instance=obj)
        if req.method == "POST":
            form = CardForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_deliver(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Deliver.objects.get(pk=id)
        form = DeliverForm(instance=obj)
        if req.method == "POST":
            form = DeliverForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_department(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Department.objects.get(pk=id)
        form = DepartmentForm(instance=obj)
        if req.method == "POST":
            form = DepartmentForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_genre(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Genre.objects.get(pk=id)
        form = GenreForm(instance=obj)
        if req.method == "POST":
            form = GenreForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_order(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Order.objects.get(pk=id)
        form = OrderForm(instance=obj)
        if req.method == "POST":
            form = OrderForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_provider(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Provider.objects.get(pk=id)
        form = ProviderForm(instance=obj)
        if req.method == "POST":
            form = ProviderForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_publisher(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Publisher.objects.get(pk=id)
        form = PublisherForm(instance=obj)
        if req.method == "POST":
            form = PublisherForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_review(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Review.objects.get(pk=id)
        form = ReviewForm(instance=obj)
        if req.method == "POST":
            form = ReviewForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_shop(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Shop.objects.get(pk=id)
        form = ShopForm(instance=obj)
        if req.method == "POST":
            form = ShopForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def edit_stock(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Stock.objects.get(pk=id)
        form = StockForm(instance=obj)
        if req.method == "POST":
            form = StockForm(req.POST, instance=obj)
            if form.is_valid():
                form.save()
                return redirect('thanks')
        return render(req, "bookshop/edit.html", locals())
    else:
        return redirect('auth')


def delete_associate(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Associate.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_associate", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_author(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Author.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_author", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_availability(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Availability.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_availability", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_basket(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Basket.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_basket", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_book(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Book.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_book", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_buyer(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Buyer.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_buyer", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_card(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Card.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_card", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_deliver(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Deliver.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_deliver", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_department(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Department.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_department", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_genre(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Genre.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_genre", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_order(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Order.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_order", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_provider(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Provider.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_provider", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_publisher(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Publisher.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_publisher", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_review(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Review.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_review", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_shop(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Shop.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_shop", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def delete_stock(req, id):
    user = authenticate(username=un, password=pw)
    if user is not None:
        obj = Stock.objects.get(pk=id)
        if req.method == "POST":
            if 'yes' in req.POST.keys() and req.POST['yes']:
                obj.delete()
                return redirect("thanks")
            else:
                return redirect("edit_stock", id=id)
        return render(req, "bookshop/delete.html", locals())
    else:
        return redirect('auth')


def show_shop_stock(request):
    user = authenticate(username=un, password=pw)
    if user is not None:
        field_list_shop = ["Адрес", "Телефон"]
        field_list_stock = ["Номер магазина", "Адрес"]
        shop_list = Shop.objects.order_by('id_shop')
        stock_list = Stock.objects.order_by('id_stock')
        shop = "add_shop"
        stock = "add_stock"
        return render(request, "bookshop/show_shop_stock.html", locals())
    else:
        return redirect('auth')

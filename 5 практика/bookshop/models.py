from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.
class Card(models.Model):
    class Meta:
        db_table = 'card'
        verbose_name = 'Card'
        verbose_name_plural = 'Cards'
    id_card = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)
    cvv = models.IntegerField()
    time = models.CharField(max_length=5)

    def __str__(self):
        return "{0}-{1}-{2}".format(self.id_card, self.name, self.second_name)

class Buyer(models.Model):
    class Meta:
        db_table = 'buyer'
        verbose_name = 'Buyer'
        verbose_name_plural = 'Buyers'
    id_buyer = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)
    mail = models.CharField(max_length=50)
    time = models.CharField(max_length=5)

    def __str__(self):
        return "{0}-{1}-{2}".format(self.id_buyer, self.name, self.second_name)

class Author(models.Model):
    class Meta:
        db_table = 'author'
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'
    id_author = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return "{0}-{1}-{2}".format(self.name, self.second_name, self.last_name)

class Provider(models.Model):
    class Meta:
        db_table = 'provider'
        verbose_name = 'Provider'
        verbose_name_plural = 'Providers'
    id_provider = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=15)
    adress = models.CharField(max_length=255)
    site = models.CharField(max_length=255)

    def __str__(self):
        return "{0} - {1}".format(self.name, self.phone)

class Genre(models.Model):
    class Meta:
        db_table = 'genre'
        verbose_name = 'Genre'
        verbose_name_plural = 'Genres'
    id_genre = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        return "{0}".format(self.name)

class Publisher(models.Model):
    class Meta:
        db_table = 'publisher'
        verbose_name = 'Publisher'
        verbose_name_plural = 'Publishers'
    id_Publisher = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    adress = models.CharField(max_length=50)
    phone = models.CharField(max_length=15)
    mail = models.CharField(max_length=50)
    site = models.CharField(max_length=50)

    def __str__(self):
        return "{0}-{1}".format(self.name, self.phone)

class Shop(models.Model):
    class Meta:
        db_table = 'shop'
        verbose_name = 'Shop'
        verbose_name_plural = 'Shops'
    id_shop = models.AutoField(primary_key=True)
    adress = models.CharField(max_length=255)
    phone = models.CharField(max_length=15)

    def __str__(self):
        return "{0}|{1}".format(self.id_shop, self.phone)

class Department(models.Model):
    class Meta:
        db_table = 'department'
        verbose_name = 'Department'
        verbose_name_plural = 'Departments'
    id_department = models.AutoField(primary_key=True)
    id_shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    name = models.CharField(max_length=60)
    about = models.CharField(max_length=70)

    def __str__(self):
        return "{0}".format(self.name)

class Stock(models.Model):
    class Meta:
        db_table = 'stock'
        verbose_name = 'Stock'
        verbose_name_plural = 'Stocks'
    id_stock = models.AutoField(primary_key=True)
    id_shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    adress = models.CharField(max_length=255)

    def __str__(self):
        return "{0}".format(self.adress)

class Associate(models.Model):
    class Meta:
        db_table = 'associate'
        verbose_name = 'Associate'
        verbose_name_plural = 'Employees'
    id_associate = models.AutoField(primary_key=True)
    id_stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    id_shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    passport = models.CharField(max_length=255)
    wage = models.FloatField()
    position = models.CharField(max_length=50)

    def __str__(self):
        return "{0} {1} {2}-{3}".format(self.last_name, self.name, self.second_name, self.position)

class Availability(models.Model):
    class Meta:
        db_table = 'availability'
        verbose_name = 'Availability'
        verbose_name_plural = 'Availabilitys'
    id_availability = models.AutoField(primary_key=True)
    id_stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    quantity = models.IntegerField()

    def __str__(self):
        return "{0}-{1}".format(self.name, self.quantity)

class Order(models.Model):
    class Meta:
        db_table = 'order'
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'
    id_order = models.AutoField(primary_key=True)
    id_associate = models.ForeignKey(Associate, on_delete=models.CASCADE)
    id_card = models.ForeignKey(Card, on_delete=models.CASCADE)
    id_buyer = models.ForeignKey(Buyer, on_delete=models.CASCADE)
    date = models.DateTimeField()

    def __str__(self):
        return "{0}-{1}".format(self.id_order, self.date)

class Deliver(models.Model):
    class Meta:
        db_table = 'deliver'
        verbose_name = 'Deliver'
        verbose_name_plural = 'Delivers'
    id_deliver = models.AutoField(primary_key=True)
    id_order = models.ForeignKey(Order, on_delete=models.CASCADE)
    adress = models.CharField(max_length=255)
    time = models.CharField(max_length=10)
    status = models.CharField(max_length=20)

    def __str__(self):
        return "{0}-{1}".format(self.id_deliver, self.adress)

class Basket(models.Model):
    class Meta:
        db_table = 'basket'
        verbose_name = 'Basket'
        verbose_name_plural = 'Baskets'
    id_basket = models.AutoField(primary_key=True)
    id_order = models.ForeignKey(Order, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.FloatField(validators=[MinValueValidator(0.1)])

    def __str__(self):
        return "{0}-{1}".format(self.id_basket, self.price)

class Book(models.Model):
    class Meta:
        db_table = 'book'
        verbose_name = 'Book'
        verbose_name_plural = 'Books'
    id_book = models.AutoField(primary_key=True)
    id_author = models.ForeignKey(Author, on_delete=models.CASCADE)
    id_provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    id_genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    id_publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    id_basket = models.ForeignKey(Basket, on_delete=models.CASCADE, default=1)
    name = models.CharField(max_length=255)
    language = models.CharField(max_length=20)
    price = models.FloatField(validators=[MinValueValidator(0.1)])
    cover = models.ImageField(blank=True, upload_to='images/book/%Y/%m/%d/', verbose_name='Ссылка картинки')

    def __str__(self):
        return "{0}-{1}".format(self.name, self.id_author.__str__())

class Review(models.Model):
    class Meta:
        db_table = 'review'
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'
    id_review = models.AutoField(primary_key=True)
    id_book = models.ForeignKey(Book, on_delete=models.CASCADE)
    text = models.CharField(max_length=255)

    def __str__(self):
        return "{0}".format(self.text)
